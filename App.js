import React from 'react';
import {Text, View} from 'react-native';
import {useSelector} from 'react-redux';

import {OnBoardingStack} from './src/navigation';

import {withTheme} from './src/components/hoc';

const App = props => {
  const auth = useSelector(state => state.auth);

  return !auth.isLoggedIn ? (
    <OnBoardingStack {...props} />
  ) : (
    <View>
      <Text>Logged in</Text>
    </View>
  );
};

export default withTheme(App);
