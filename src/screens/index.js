import Splash from './Splash';
import OnBoard from './OnBoard';
import Login from './Login';

export {Splash, OnBoard, Login};
