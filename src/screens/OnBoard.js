import React, {useState} from 'react';
import {
  View,
  Image,
  StatusBar,
  TouchableOpacity,
  StyleSheet,
  Text,
} from 'react-native';
import {Button} from 'react-native-paper';

import {Typography} from '../components';

import {IMAGES, SIZES} from '../constants';

const OnBoard = ({navigation, route}) => {
  const {theme} = route.params;

  const [step, setStep] = useState(1);

  const renderTitle = value => {
    switch (value) {
      case 1:
        return 'Committed delivery on time';
      case 2:
        return '24/7 Fastest and safest delivery';
      case 3:
        return 'Track your delivery in real time';
      case 4:
        return 'Your package in our safe hands';
    }
  };

  const renderImage = value => {
    switch (value) {
      case 1:
        return IMAGES.ONBOARDING1;
      case 2:
        return IMAGES.ONBOARDING2;
      case 3:
        return IMAGES.ONBOARDING3;
      case 4:
        return IMAGES.ONBOARDING4;
    }
  };

  const handleSkip = () => navigation.navigate('Login');

  const handleNext = () => {
    step < 4 ? setStep(step + 1) : handleSkip();
  };

  const setWidth = i => {
    return i + 1 === step ? 40 : 20;
  };

  return (
    <View style={[styles.container, {backgroundColor: theme.bg}]}>
      <StatusBar backgroundColor={theme.bg} barStyle={theme.status} />
      <Typography variant="body1" family="bold" color={theme.font}>
        {renderTitle(step)}
      </Typography>
      <Image source={renderImage(step)} style={styles.image} />
      <View style={styles.indicators}>
        {[...Array(4)].map((x, i) => {
          return (
            <View
              key={i}
              style={[
                styles.indicator,
                {width: setWidth(i), backgroundColor: theme.primary},
              ]}>
              <Text> </Text>
            </View>
          );
        })}
      </View>
      <View style={styles.btnContainer}>
        <TouchableOpacity style={styles.skip} onPress={handleSkip}>
          <Typography color={theme.font}>Skip</Typography>
        </TouchableOpacity>
        <Button
          mode="contained"
          color={theme.primary}
          onPress={handleNext}
          style={styles.btn}>
          {step < 4 ? 'Next' : 'Get Start'}
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: '100%',
    marginVertical: SIZES.margin * 6,
  },
  indicators: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 100,
    left: 20,
  },
  indicator: {
    marginRight: 5,
    borderRadius: SIZES.radius,
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '90%',
    position: 'absolute',
    bottom: 30,
  },
  skip: {
    justifyContent: 'flex-start',
  },
  btn: {
    justifyContent: 'flex-end',
    borderRadius: 25,
  },
});

export default OnBoard;
