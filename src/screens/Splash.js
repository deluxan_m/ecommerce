import React, {useEffect} from 'react';
import {View, Image, StatusBar, StyleSheet} from 'react-native';

import {COLORS, IMAGES} from '../constants';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('OnBoard');
    }, 1500);
  });

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={COLORS.primary} />
      <Image source={IMAGES.LOGO_WHITE} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primary,
  },
});

export default Splash;
