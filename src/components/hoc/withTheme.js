import React from 'react';
import {useSelector} from 'react-redux';

import {COLORS, COLORS_DARK} from '../../constants';

const withTheme = Comp => {
  const withThemeComp = props => {
    const theme = useSelector(state => state.theme);

    const {isLight} = theme;
    return <Comp theme={isLight ? COLORS : COLORS_DARK} {...props} />;
  };
  return withThemeComp;
};

export default withTheme;
