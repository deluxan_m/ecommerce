import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {Splash, OnBoard, Login} from '../../screens';

const Stack = createStackNavigator();

const OnBoardingStack = ({theme}) => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={Splash} initialParams={{theme}} />
      <Stack.Screen
        name="OnBoard"
        component={OnBoard}
        initialParams={{theme}}
      />
      <Stack.Screen name="Login" component={Login} initialParams={{theme}} />
    </Stack.Navigator>
  );
};

export default OnBoardingStack;
