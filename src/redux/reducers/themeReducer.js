const INITIAL_STATE = {
  isLight: false,
};

const themeReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default themeReducer;
