export const COLORS = {
  primary: '#E04428',
  white: '#ffffff',
  bg: '#ffffff',
  font: '#2B2A2A',
  status: 'dark-content',
};

export const COLORS_DARK = {
  primary: '#E04428',
  white: '#ffffff',
  bg: '#000000',
  font: '#ffffff',
  status: 'light-content',
};
