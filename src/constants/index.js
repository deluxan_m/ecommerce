import {IMAGES} from './images';
import {COLORS, COLORS_DARK} from './colors';
import {SIZES} from './sizes';

export {IMAGES, COLORS, COLORS_DARK, SIZES};
