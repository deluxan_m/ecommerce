import LOGO_ORANGE from '../../assets/images/logo_orange.png';
import LOGO_BLUE from '../../assets/images/logo_blue.png';
import LOGO_WHITE from '../../assets/images/logo_white.png';

import ONBOARDING1 from '../../assets/images/onboarding1.png';
import ONBOARDING2 from '../../assets/images/onboarding2.png';
import ONBOARDING3 from '../../assets/images/onboarding3.png';
import ONBOARDING4 from '../../assets/images/onboarding4.png';

export const IMAGES = {
  LOGO_ORANGE,
  LOGO_BLUE,
  LOGO_WHITE,
  ONBOARDING1,
  ONBOARDING2,
  ONBOARDING3,
  ONBOARDING4,
};
